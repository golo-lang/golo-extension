module hello


----
TODO:
 - ajouter les méthode associée aux types golo (ex méthode des listes)
   - utiliser "variable.legacy.builtin.golo" / à voir
 - ajouter les modules golo existants
----
import something
import gololang.Errors

struct Person = { name, age, email }

augmentation FooBar = {
  function foo = |this| -> "foo"
  function bar = |this, a| -> this: length() + a
}
 
@hello
@option
@result
function hello = |message| {
  42: times({
    println("say hello")
  })
  return "👋 " + message
}

# this is a comment
# TODO: something
# NOTE: Golo rocks ❤️
# this is a comment

----
this is a comment  
NOTE: this is the main program
----
function main = |args| {
  return
  null
  true
  false

  let text = """
  this is a text
  
  """

  if bob is "ok" {
    let buddies = list("🐼", "🐱", "🐻")
  } else {
    
  }

  try catch finally


  Dynamicobject
  DynamicObject

  var bob = Option("Bob")
  let sam = Result("Sam")

  let port =  sysEnv("HTTP_PORT"): either(
    default= -> 9090, 
    mapping= |value|-> Integer.parseInt(value)
  )

  this: name()
  self: ho()

  let res = trying({
    let f = foo()
    let x = bar()
    let z = f(x)
    return z + plop()

    println("Hello") # cf class java correspondante

    hello world

    Hello World

    12.0 + 56 = 67
  })

}